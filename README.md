## About

This is the query and authoring interface for the ISMI CIDOC-CRM data based on the [ResearchSpace](https://researchspace.org/) platform.

## Requirements

You will need [Docker](https://www.docker.com/) installed on your computer.

### First time run

Open the Terminal and navigate to the repository.

Optionally you can create a `docker-compose.override.yml` file based on `docker-compose.override.yml.template` and override options in the standard `docker-compose.yml` file:

```
cp docker-compose.override.yml.template docker-compose.override.yml
```

Then build and start the environment with

```
docker compose up -d
```

Navigate to http://localhost:8080/. You should see the researchspace platform and be able to login with the initial username 'admin' and password 'admin'.

### Stop and restart

To stop, execute in the Terminal
```
docker compose stop
```

To restart, type
```
docker compose start
```

### Deleting

To delete the containers, type
```
docker compose down
```

To delete the containers along with all the non-persistent data volumes, type
```
docker compose down -v
```

### Development

- HTML templates for the ResearchSpace app are in `researchspace/apps/ismi/data/templates/`
- Trig fields for the ResearchSpace app are in `researchspace/apps/ismi/ldp/assets/`
  - Trig fields can be generated from YAML definitions in `ismi-fields/` using https://pypi.org/project/semantic-field-definition-generator/

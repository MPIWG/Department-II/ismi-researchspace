@prefix crm: <http://www.cidoc-crm.org/cidoc-crm/> .
@prefix ldp: <http://www.w3.org/ns/ldp#> .
@prefix rsfield: <http://www.researchspace.org/resource/system/fields/> .
@prefix prov: <http://www.w3.org/ns/prov#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix sp: <http://spinrdf.org/sp#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix rsuser: <http://www.researchspace.org/resource/user/> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .

<http://www.researchspace.org/resource/system/fieldDefinitionContainer/context> {
  <http://www.researchspace.org/resource/system/fieldDefinitionContainer> rdfs:comment "Container to store field definitions.";
    a ldp:Container, ldp:Resource, prov:Entity ;
    rdfs:label "Form Container";
    prov:wasAttributedTo rsuser:admin ;
    prov:generatedAtTime "2020-04-06T13:49:19.238+03:00"^^xsd:dateTime .
}


    <http://templates.mpiwg-berlin.mpg.de/ismi/fieldDefinition/misidentification_person/context> {

        <http://www.researchspace.org/resource/system/fieldDefinitionContainer> ldp:contains <http://templates.mpiwg-berlin.mpg.de/ismi/fieldDefinition/misidentification_person> .
 
        <http://templates.mpiwg-berlin.mpg.de/ismi/fieldDefinition/misidentification_person> 
            rdfs:label "Misidentified author" ;
            rsfield:domain crm:E13_Attribute_Assignment ; 
                rsfield:selectPattern <http://templates.mpiwg-berlin.mpg.de/ismi/fieldDefinition/misidentification_person/query/select> ;
                rsfield:insertPattern <http://templates.mpiwg-berlin.mpg.de/ismi/fieldDefinition/misidentification_person/query/insert> ;
                rsfield:autosuggestionPattern <http://templates.mpiwg-berlin.mpg.de/ismi/fieldDefinition/misidentification_person/query/autosuggestion> ;
    
        a rsfield:Field, ldp:Resource, prov:Entity ;
        prov:wasAttributedTo rsuser:admin ;
        prov:generatedAtTime "2021-01-08T11:43:46.111Z"^^xsd:dateTime .

                <http://templates.mpiwg-berlin.mpg.de/ismi/fieldDefinition/misidentification_person/query/select> a sp:Query;
                    sp:text """SELECT ?value ?label WHERE {
  $subject a ismi:Misidentification .
  $subject crm:P141_assigned / frbroo:R9i_realises / frbroo:R16i_was_initiated_by / crm:P14_carried_out_by ?value.
  ?value rdfs:label ?label.
}""" .
                <http://templates.mpiwg-berlin.mpg.de/ismi/fieldDefinition/misidentification_person/query/insert> a sp:Query;
                    sp:text """INSERT { 
  $subject crm:P141_assigned ?expression.
  ?expression a frbroo:F22_Self-Contained_Expression;
      frbroo:R9i_realises ?text.
  ?text a frbroo:F14_Individual_Work;
      frbroo:R16i_was_initiated_by ?creation.
  ?creation a frbroo:F27_Work_Conception;
      crm:P14_carried_out_by $value.
} WHERE {
  bind(uri(concat(str($subject), "/witness/expression")) as ?expression)
  bind(uri(concat(str($subject), "/text")) as ?text)
  bind(uri(concat(str($subject), "/text/creation")) as ?creation)
}""" .
                <http://templates.mpiwg-berlin.mpg.de/ismi/fieldDefinition/misidentification_person/query/autosuggestion> a sp:Query;
                    sp:text """SELECT ?value ?label WHERE {
  ?value a ismi:Person.
  
  ?search_hit bds:search ?__token__ ;
    bds:relevance ?score .
  
  {
    # match person label
    ?value rdfs:label ?search_hit.
  } union {
    # match ismi id
    ?value crm:P1_is_identified_by ?id.
    ?id crm:P2_has_type <http://content.mpiwg-berlin.mpg.de/ns/ismi/type/identifier/ismi-id>;
      rdfs:label ?search_hit.
  }    

  # label
  ?value rdfs:label ?p_label.
  # ismi id
  ?value crm:P1_is_identified_by ?id.
  ?id crm:P2_has_type <http://content.mpiwg-berlin.mpg.de/ns/ismi/type/identifier/ismi-id>.
  ?id rdfs:label ?id_label.
  # optional birth and death date
  optional {
    ?value crm:P98i_was_born / crm:P4_has_time-span / (crm:P82_at_some_time_within | crm:P82a_begin_of_the_begin) ?bdate.
  }
  optional {
    ?value crm:P100i_died_in / crm:P4_has_time-span / (crm:P82_at_some_time_within | crm:P82a_begin_of_the_begin) ?ddate.
  }
  # label with birth and death date
  bind(if(bound(?bdate)||bound(?ddate), concat(" (", coalesce(concat("*G", str(year(?bdate))), ""), coalesce(concat("†G", str(year(?ddate))), ""), ")"), "") as ?date_label)
  # label with dates and ismi-id
  bind(concat(?p_label, " [", ?id_label, "] ", ?date_label) as ?label)
} 
ORDER BY DESC(?score) LIMIT 100""" .
    
    }
#!/bin/bash

# send errors to sentry
export SENTRY_DSN='https://99890d4844ea41c48f13e21bdb5358db@sentry.mpiwg-berlin.mpg.de/6'
eval "$(scripts/sentry-cli bash-hook)"

# 
# run backup of blazegraph and export and update of dataset
#

# backup directory in blazegraph container
BACKUPDIR=/backup-data
# export directory in tools container
EXPORTDIR=/export-data
# user id for tools container
TOOLS_USER="1000"

# run ismi data updates
echo "Running graph updates..."
docker compose run --rm --no-TTY --user $TOOLS_USER tools graph/update_graph.py --log-to STDOUT 'http://blazegraph:8080/blazegraph/namespace/kb/sparql'

echo "Starting export/backup at $(date)"

# stop services
echo "Stopping services..."
docker compose rm --stop --force researchspace

# run private export
echo "Creating private export..."
PRIVATE_DUMP_DIR=$EXPORTDIR/daily-data/private
docker compose run --rm --no-TTY --user $TOOLS_USER --entrypoint /bin/sh tools -c "mkdir -p $PRIVATE_DUMP_DIR"
docker compose run --rm --no-TTY --user $TOOLS_USER tools graph/ismirdf2model.py --log-to STDOUT -u 'http://blazegraph:8080/blazegraph/namespace/kb/sparql' "$PRIVATE_DUMP_DIR/ismi-full.gpickle"
docker compose run --rm --no-TTY --user $TOOLS_USER tools graph/model2neo4j_import.py --log-to STDOUT --nodes-file "$PRIVATE_DUMP_DIR/neo4j-nodes.csv" --relations-file "$PRIVATE_DUMP_DIR/neo4j-relations.csv" "$PRIVATE_DUMP_DIR/ismi-full.gpickle"

# run public export
echo "Creating public export..."
PUBLIC_DUMP_DIR=$EXPORTDIR/daily-data/public
docker compose run --rm --no-TTY --user $TOOLS_USER --entrypoint /bin/sh tools -c "mkdir -p $PUBLIC_DUMP_DIR"
docker compose run --rm --no-TTY --user $TOOLS_USER tools graph/export_public_graph.py --format nt --log-to STDOUT "$PUBLIC_DUMP_DIR/ismi-public.nt" 'http://blazegraph:8080/blazegraph/namespace/kb/sparql'

# create public export XML files
echo "Creating public export XML version..."
# clean up eventual temporary container
docker rm -f blazegraph-tmp
# start temporary blazegraph for public data
docker run --rm --detach --name blazegraph-tmp --network ismi-researchspace_default --volume 'ismi-researchspace_export-data:/export-data' robcast/researchspace-blazegraph:2.2.0-20160908
while ! docker exec blazegraph-tmp curl -fs 'http://blazegraph-tmp:8080/blazegraph/namespace/kb/sparql' -o /dev/null
do
  echo "  waiting for blazegraph..."
  sleep 2
done
# import data
docker exec blazegraph-tmp curl -fs 'http://blazegraph-tmp:8080/blazegraph/namespace/kb/sparql' -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' --data "uri=file://$PUBLIC_DUMP_DIR/ismi-public.nt"
# export model and (split) xml files
echo "Exporting public data..."
docker compose run --rm --no-TTY --user $TOOLS_USER tools graph/ismirdf2model.py --log-to STDOUT -u 'http://blazegraph-tmp:8080/blazegraph/namespace/kb/sparql' "$PUBLIC_DUMP_DIR/ismi-public.gpickle"
docker compose run --rm --no-TTY --user $TOOLS_USER tools graph/model2ismixml.py --log-to STDOUT "$PUBLIC_DUMP_DIR/ismi-public.gpickle" "$PUBLIC_DUMP_DIR/openmind-data.xml"
docker compose run --rm --no-TTY --user $TOOLS_USER tools graph/ismixml_splitter.py --log-to STDOUT "$PUBLIC_DUMP_DIR/openmind-data.xml" "$PUBLIC_DUMP_DIR/openmind-data-%s.xml"
docker compose run --rm --no-TTY --user $TOOLS_USER tools graph/model2neo4j_import.py --log-to STDOUT --nodes-file "$PUBLIC_DUMP_DIR/neo4j-nodes.csv" --relations-file "$PUBLIC_DUMP_DIR/neo4j-relations.csv" "$PUBLIC_DUMP_DIR/ismi-public.gpickle"
docker stop blazegraph-tmp

# run offline blazegraph backup into /back-data/
docker compose stop blazegraph
GRAPH_DUMP_DIR=$BACKUPDIR/$(/bin/date +'%m/%d')
echo "Creating backup..."
docker compose run --rm --no-TTY --user jetty --entrypoint /bin/sh blazegraph -c "mkdir -p $GRAPH_DUMP_DIR"
docker compose run --rm --no-TTY --user jetty --entrypoint /usr/local/openjdk-11/bin/java blazegraph -cp 'webapps/blazegraph/WEB-INF/lib/*' -Dlog4j.configuration=file:/var/lib/jetty/webapps/blazegraph/WEB-INF/classes/log4j.properties com.bigdata.rdf.sail.ExportKB -outdir "$GRAPH_DUMP_DIR" -format N-Quads /config/RWStore.properties

# restart services
docker compose up -d

echo "Finished backup at $(date)"

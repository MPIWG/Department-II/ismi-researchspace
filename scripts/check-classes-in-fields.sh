#!/bin/bash

files=ismi-fields/*.yml
#files=researchspace/apps/ismi/data/templates/*.html

# output unique list of all crm: uris 
grep -hEo 'crm:[-_.[:alnum:]]+' $files | sort | uniq | while read uri
do
    class=${uri:4}
    #echo "check $class"
    if ! grep -q $class researchspace/apps/ismi/ldp/assets/http%3A%2F%2Fwww.cidoc-crm.org%2Fcidoc-crm.trig
    then
        echo "$class not in CRM ontology!"
    fi
done

grep -hEo 'frbroo:[-_.[:alnum:]]+' $files | sort | uniq | while read uri
do
    class=${uri:4}
    #echo "check $class"
    if ! grep -q $class researchspace/apps/ismi/ldp/assets/http%3A%2F%2Fiflastandards.info%2Fns%2Ffr%2Ffrbr%2Ffrbroo.trig
    then
        echo "$class not in FRBRoo ontology!"
    fi
done

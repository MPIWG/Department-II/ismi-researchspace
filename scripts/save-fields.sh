#!/bin/bash
# read field definitions from rs runtime into ismi-fields
semantic-field-util read -y ismi-fields -t data/researchspace/ldp/assets --split-fields --field-id-prefix 'http://templates.mpiwg-berlin.mpg.de/ismi/fieldDefinition/'
# write fields to ismi app
semantic-field-util write -y ismi-fields -t researchspace/apps/ismi/ldp/assets --split-fields --field-id-prefix 'http://templates.mpiwg-berlin.mpg.de/ismi/fieldDefinition/' --add-ns-prefix 'skos=http://www.w3.org/2004/02/skos/core#'
